#Adjust format of data, send info to InfluxDB: 

import datetime
import write_influxdb

def format_send_data(data):
    db_data = []
    # Get current datetime
    now = datetime.datetime.now()
    # Convert datetime to Unix timestamp in nanoseconds
    timestamp_ns = int(now.timestamp() * 1e9)

    for d in data:
        if d.get('Status').startswith('Up'):
            field = 1
        else:
            field = 0
        
        db_data.append(
            {
                'measurement': 'Status_of_APs',
                'tags': {'Switch IP': d.get('Switch IP'), 'Name': d.get('Name')},
                'time': timestamp_ns,
                'fields': {'Status': int(field)}
            }
        )
    
    try:
#        print(db_data)
        write_influxdb.write_data_db(db_data)
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {str(e)}")
