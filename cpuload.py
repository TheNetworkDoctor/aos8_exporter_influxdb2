#Adjust format of data, send info to InfluxDB: 

import time
import datetime
import write_influxdb

def format_send_data(vMM,data):
    db_data= list()
    # Get current datetime
    now = datetime.datetime.now()
    # Convert datetime to Unix timestamp in nanoseconds
    timestamp_ns = int(now.timestamp() * 1e9)

    data = data[0].split(', ')

    for d in data:
        if d.startswith('idle') == True:
            data = d.lstrip('idle ')
            break
    
    d = data.strip('%')
    field = round (100 - float(d),1) 

    db_data = [ 
                { 
                    'measurement':'cpuload',
                    'tags':{'Switch IP':vMM},
                    'time': timestamp_ns,
                    'fields': {'Status':field}
                }
            ]
    try:
#        print(db_data)
        write_influxdb.write_data_db(db_data)
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {str(e)}")
