
import time
import datetime
import write_influxdb

def format_send_data(data, AP):
    field = len(data)
    db_data= list()
    # Get current datetime
    now = datetime.datetime.now()
    # Convert datetime to Unix timestamp in nanoseconds
    timestamp_ns = int(now.timestamp() * 1e9)

    db_data = [ 
                    { 
                        'measurement':'Number_Associations_APs',
                        'tags':{'Name':AP},
                        'time': timestamp_ns,
                        'fields': {'Num Clients':int(field)}
                    }
                 ]
    try:
#        print(db_data)
        write_influxdb.write_data_db(db_data)
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {str(e)}")
