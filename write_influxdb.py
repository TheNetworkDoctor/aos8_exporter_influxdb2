
#Write data Influx Database

import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

# Define the initial hardcoded values
default_influxdb_url = "http://10.1.1.1:8086"
default_influxdb_token = 'supersecretadmintoken'
default_influxdb_db_name = 'custom-database-name'
default_influxdb_org_name = 'custom-org-name'

# Retrieve values from environment variables if they are set
influxdb_url = os.environ.get("INFLUXDB_URL", default_influxdb_url)
influxdb_token = os.environ.get("INFLUXDB_TOKEN", default_influxdb_token)
influxdb_db_name = os.environ.get("INFLUXDB_DB_NAME", default_influxdb_db_name)
influxdb_org_name = os.environ.get("INFLUXDB_ORG_NAME", default_influxdb_org_name)

def write_data_db(data):
    # Initialize the InfluxDB 2.0 client
    client = influxdb_client.InfluxDBClient(url=influxdb_url, token=influxdb_token, org=influxdb_org_name)

    # Write data to InfluxDB 2.0
    write_api = client.write_api(write_options=SYNCHRONOUS)
    
    # Write each data point to InfluxDB
    for point in data:
        try:
            write_api.write(bucket=influxdb_db_name, record=point)
        except Exception as e:
            print(f"Failed to write data point: {point}. Error: {str(e)}")
