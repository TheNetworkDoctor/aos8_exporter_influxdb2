import time
import datetime
import write_influxdb

def format_send_data(data, AP, radio):
    db_data = []
    # Get current datetime
    now = datetime.datetime.now()
    # Convert datetime to Unix timestamp in nanoseconds
    timestamp_ns = int(now.timestamp() * 1e9)

    if radio == 0:
        radio = '5Ghz'
    else:
        radio = '2.4Ghz'

    tag1 = next((d for d in data if d.get('Parameter') == 'Rx Data Bytes'), None)
    tag2 = next((d for d in data if d.get('Parameter') == 'Tx Data Bytes Transmitted'), None)
    tag3 = next((d for d in data if d.get('Parameter') == 'Rx CRC Errors'), None)

    if not all([tag1, tag2, tag3]):
        print("Missing required parameters in data.")
        return

    db_data = [
        {
            'measurement': 'Bandwidth_Consumed_CRCs',
            'tags': {'Name': AP, 'Radio': radio},
            'time': timestamp_ns,
            'fields': {
                'Rx Data Bytes': int(tag1.get('Value')),
                'Tx Data Bytes Transmitted': int(tag2.get('Value')),
                'Rx CRC Errors': int(tag3.get('Value'))
            }
        }
    ]

    try:
#        print(db_data)
        write_influxdb.write_data_db(db_data)
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {str(e)}")
