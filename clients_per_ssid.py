import datetime
import write_influxdb

def format_send_data(MC, data):
    # Get current datetime
    now = datetime.datetime.now()
    # Convert datetime to Unix timestamp in nanoseconds
    timestamp_ns = int(now.timestamp() * 1e9)

    db_data = []
    
    for d in data:
        db_data.append(
            {
                'measurement': 'Number_Clients_SSID',
                'tags': {'Switch IP': MC, 'ESSID': d.get('ESSID')},
                'time': timestamp_ns,
                'fields': {'Clients': int(d.get('Clients', 0))}
            }
        )
    
    try:
#        print(db_data)
        write_influxdb.write_data_db(db_data)
    except Exception as e:
        print(f"Failed to write data to InfluxDB: {str(e)}")
